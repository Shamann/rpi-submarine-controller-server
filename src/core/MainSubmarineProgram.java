package core;

import javax.naming.TimeLimitExceededException;

import com.pi4j.io.gpio.RaspiPin;

public class MainSubmarineProgram {

	// Connect to client application upon creation...
	private static MessageReceiver messenger = new MessageReceiver(20000);
	
	public static void main(String[] args) {
		// If not connected try again - give the client more time to start...
		if (messenger.isConnectedToClient()) {	
		} else {
			try {
				// If the connection is not established, close the application with error code 1.
				messenger.authenticateWithClient(10000);
			} catch (TimeLimitExceededException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		// Sets the controllers for lights and motors
		LightsController lights = new LightsController(28, RaspiPin.GPIO_27, RaspiPin.GPIO_29);
		MotorsController motors = new MotorsController();
		// Sets pins for the actually used/connected motors
		motors.setElevationMotor(2, RaspiPin.GPIO_00, RaspiPin.GPIO_03);
		motors.setLeftMotor(9, RaspiPin.GPIO_08, RaspiPin.GPIO_07);
		motors.setRightMotor(13, RaspiPin.GPIO_12, RaspiPin.GPIO_14);
		// Turns on the lights
		lights.switchOn();
		// Infinite loop to receive messages and change them to movement
		// In few days I should add the possibility to dim the lights any time
		while (true) {
			Message message = messenger.receive();
			if (message != null) {
				if (message.isExitMessage()) {
					lights.switchOf();
					DeviceController.shutDown();
					break;
				}
				switch(message.getDirection()) {
				case UP : motors.goUp(message.isMoving()); break;
				case DOWN : motors.goDown(message.isMoving()); break;
				case FORWARD : motors.goForward(message.isMoving()); break;
				case BACKWARD : motors.goBackward(message.isMoving()); break;
				case LEFT : motors.turnLeft(message.isMoving()); break;
				case RIGHT : motors.turnRight(message.isMoving()); break;
				}
			}
		}
	}
	

}
