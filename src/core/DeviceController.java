package core;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.wiringpi.Gpio;

/**
 * Abstract class - every GPIO device controller must extend this class to omit
 * duplicity of GpioController objects and multiple wiring of GPIO pins
 * 
 * @author Lukas
 *
 */
public abstract class DeviceController {

	protected static final GpioController gpio = GpioFactory.getInstance();
	private static boolean wiringPiSetupPerformed = false;

	public DeviceController() {
		if (!wiringPiSetupPerformed) {
			if (Gpio.wiringPiSetup() == -1) {
				System.exit(1);
			}
			wiringPiSetupPerformed = true;
			System.out.println("System: gpio wired");
		}
	}

	/**
	 * throws exception - (not solved) not a problem to run and restart the
	 * program any time
	 */
	public static void shutDown() {
		gpio.shutdown();
	}
}
