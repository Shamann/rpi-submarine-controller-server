package core;

/**
 * Message class that stores information about required direction and movement.
 * Also contains boolean value quitStatus whether the client closed connection
 * @author Lukas Hudec
 */
public class Message {
	public enum Direction {
		UP, DOWN, FORWARD, BACKWARD, LEFT, RIGHT;
	}
	
	private Direction direction;
	private boolean movement;
	private boolean quitStatus;
	
	/**
	 * Creates default quitting message
	 */
	public Message() {
		quitStatus = true;
	}
	
	/**
	 * Creates message containing information about required direction and movement
	 * @param direction from enumeration Direction
	 * @param movement start/stop
	 */
	public Message(Direction direction, boolean movement) {
		this.direction = direction;
		this.movement = movement;
		quitStatus = false;
	}

	@Override
	public String toString() {
		return (quitStatus ? "Message [close]" : "Message [direction=" + direction.toString() + ", movement=" + movement + "]");
	}

	public Direction getDirection() {
		return direction;
	}

	public boolean isMoving() {
		return movement;
	}

	public boolean isExitMessage() {
		return quitStatus;
	}
	
	
}
