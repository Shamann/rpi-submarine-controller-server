package core;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.wiringpi.SoftPwm;

/**
 * Controller of connected lights.
 * 
 * @author Lukas Hudec
 */
public class LightsController extends DeviceController {

	private boolean light;
	private final int PIN_ENABLE;
	private final GpioPinDigitalOutput ledPin1;
	private final GpioPinDigitalOutput ledPin2;

	public LightsController(int enablePin, Pin outputPinA, Pin outputPinB) {
		super();
		PIN_ENABLE = enablePin;
		ledPin1 = gpio.provisionDigitalOutputPin(outputPinA, PinState.LOW);
		ledPin2 = gpio.provisionDigitalOutputPin(outputPinB, PinState.LOW);

		SoftPwm.softPwmCreate(PIN_ENABLE, 0, 100);
	}

	public void switchOn() {
		light = true;
		ledPin1.high();
		ledPin2.low();
		SoftPwm.softPwmWrite(PIN_ENABLE, 100);
		System.out.println("System: lights on");
	}

	public void switchOf() {
		if (light) {
			light = false;
			ledPin1.low();
			ledPin2.low();
			SoftPwm.softPwmWrite(PIN_ENABLE, 0);
			System.out.println("System: lights off");
		}
	}

	/**
	 * Dimms the lights to the % of original power.
	 * If power is lesser than 0 or greater than 100 - value is truncated to borders
	 * @param power percentage value interval <0;100>
	 */
	public void dimLights(int power) {
		if (light) {
			SoftPwm.softPwmWrite(PIN_ENABLE, power <= 100 ? (power >= 0 ? power : 0) : 100);
		}
	}
}
