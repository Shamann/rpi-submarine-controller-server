package core;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import javax.naming.TimeLimitExceededException;

import core.Message.Direction;

public class MessageReceiver {

	private final String USERNAME = "pi";
	private final String PASSWORD = "raspberry";
	private DatagramSocket socket;
	private DatagramPacket incomingMessage;
	private DatagramPacket outgoingMessage;
	private static InetAddress myIPAddress;
	private InetAddress controllerAddress;
	private final int PORT = 28050;
	private boolean isConnected = false;

	public MessageReceiver(int timeout) {
		try {
			myIPAddress = getFirstNonLoopbackAddress();
			socket = new DatagramSocket(PORT, myIPAddress);
		} catch (SocketException e) { 
			e.printStackTrace();
		}
		System.out.println(myIPAddress);
		try {
			authenticateWithClient(timeout);
		} catch (TimeLimitExceededException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Waits to message from client application. If the message contains required authentication information
	 * program sends "approved" message back to client.
	 * 
	 * @param timeout Time to wait for message to receive.
	 * @throws TimeLimitExceededException
	 */
	public void authenticateWithClient(int timeout) throws TimeLimitExceededException {
		byte[] message = new byte[100];
		incomingMessage = new DatagramPacket(message, message.length);
		
		try {
			socket.setSoTimeout(timeout);
			socket.receive(incomingMessage);
			System.out.println("System: Message recieved...");
		} catch (IOException e) {
			isConnected = false;
			throw new TimeLimitExceededException("Wait time limit exceeded");
		}
		controllerAddress = incomingMessage.getAddress();
		String data = new String(incomingMessage.getData());
		String[] dataSplit = data.split("@");
		String[] credentials = dataSplit[0].split(":");
		
		if (credentials[0].equalsIgnoreCase(USERNAME) && credentials[1].equalsIgnoreCase(PASSWORD)) {
			System.out.println("System: Credentials OK");
			String request = dataSplit[1].replaceAll("\u0000", "");
			
			if (request.equalsIgnoreCase("requestingConnection")) {
				System.out.println("System: Client Authentification OK");				
				String response = "approved";
				outgoingMessage = new DatagramPacket(response.getBytes(), response.getBytes().length, controllerAddress, PORT);
				
				try {
					socket.send(outgoingMessage);
					System.out.println("System: Authentification response sent");
					isConnected = true;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public boolean isConnectedToClient() {
		return isConnected;
	}
	
	public static InetAddress getFirstNonLoopbackAddress() throws SocketException {
		if (myIPAddress == null) {
			Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
			while (en.hasMoreElements()) {
				NetworkInterface i = (NetworkInterface) en.nextElement();
				for (Enumeration<InetAddress> en2 = i.getInetAddresses(); en2
						.hasMoreElements();) {
					InetAddress addr = (InetAddress) en2.nextElement();
					if (!addr.isLoopbackAddress()) {
						if (addr instanceof Inet4Address) {
							return addr;
						}
					}
				}
			}
		} else {
			return myIPAddress;
		}
	    return null;
	}

	/**
	 * Receives a message from client application. When this method returns,
	 * returned Message is filled with parsed received data. The Message also
	 * may contain the information about termination of client application.
	 * 
	 * This method blocks until a Message is received. If does not receive 
	 * anything in 15s throws an InterruptedException
	 * 
	 * @return parsed message with received data. If message does not contains information about termination or movement
	 * returns null
	 */
	public Message receive() {
		byte[] buffer = new byte[100];
		incomingMessage = new DatagramPacket(buffer, buffer.length);
		
		try {
			socket.receive(incomingMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String data[] = new String(incomingMessage.getData()).split(":");
		
		if (data.length < 2) return null;
		
		System.out.println(data[0] + " " + data[1]);
		
		if (data[0].equalsIgnoreCase("close")) {
			return new Message();
		}
		
		boolean movement = data[1].contains("1");
		
		if (data[0].equalsIgnoreCase(Direction.UP.toString())) {
			return new Message(Direction.UP,movement);
		} else if (data[0].equalsIgnoreCase(Direction.DOWN.toString())) {
			return new Message(Direction.DOWN,movement);
		} else if (data[0].equalsIgnoreCase(Direction.FORWARD.toString())) {
			return new Message(Direction.FORWARD,movement);
		} else if (data[0].equalsIgnoreCase(Direction.BACKWARD.toString())) {
			return new Message(Direction.BACKWARD,movement);
		} else if (data[0].equalsIgnoreCase(Direction.LEFT.toString())) {
			return new Message(Direction.LEFT,movement);
		} else if (data[0].equalsIgnoreCase(Direction.RIGHT.toString())) {
			return new Message(Direction.RIGHT,movement);
		} 
		return null;
	}
	
	
}
