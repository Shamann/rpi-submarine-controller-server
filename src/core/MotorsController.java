package core;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.wiringpi.SoftPwm;

/**
 * Controller of connected motors. Supplies 2 side motors and 1 elevation motor.
 * Use setters to set the motors pins on RPi GPIO.
 * 
 * @author Lukas Hudec
 *
 */
public class MotorsController extends DeviceController {
	
	protected enum Elevation {
		UP(1),	STOP(0), DOWN(-1);
		
		private int elevation;
		private Elevation(int e) {
			elevation = e;
		}
		public int value() {
			return elevation;
		}
	}
	
	private Motor elevationMotor;
	private Motor sideMotorLeft;
	private Motor sideMotorRight;
	
	private int elevation = Elevation.STOP.value();
	private boolean FORWARD = false;
	private boolean BACKWARD = false;
	private boolean LEFT = false;
	private boolean RIGHT = false;
	
	public void setElevationMotor(int pin_enable, Pin motorPin1, Pin motorPin2) {
		elevationMotor = new Motor(pin_enable, motorPin1, motorPin2);
	}
	
	public void setLeftMotor(int pin_enable, Pin motorPin1, Pin motorPin2) {
		sideMotorLeft = new Motor(pin_enable, motorPin1, motorPin2);
		
	}
	
	public void setRightMotor(int pin_enable, Pin motorPin1, Pin motorPin2) {
		sideMotorRight = new Motor(pin_enable, motorPin1, motorPin2);
		
	}
	
	private void stopMovement() throws InterruptedException {
		sideMotorLeft.stop();
		sideMotorRight.stop();
	}
	
	private void forwardMovement() throws InterruptedException {
		sideMotorLeft.rotateClockWise();
		sideMotorRight.rotateClockWise();
	}
	
	private void backwardMovement() throws InterruptedException {
		sideMotorLeft.rotateAntiClockWise();
		sideMotorRight.rotateAntiClockWise();
	}
	
	private void forwardLeftMovement() throws InterruptedException {
		sideMotorLeft.stop();
		sideMotorRight.rotateClockWise();
	}
	
	private void forwardRightMovement() throws InterruptedException {
		sideMotorRight.stop();
		sideMotorLeft.rotateClockWise();
	}
	
	private void backwardRightMovement() throws InterruptedException {
		sideMotorRight.stop();
		sideMotorLeft.rotateAntiClockWise();
	}
	
	private void backwardLeftMovement() throws InterruptedException {
		sideMotorLeft.stop();
		sideMotorRight.rotateAntiClockWise();
	}
	
	private void rightRotate() throws InterruptedException {
		sideMotorLeft.rotateAntiClockWise(50);
		sideMotorRight.rotateClockWise(50);
	}
	
	private void leftRotate() throws InterruptedException {
		sideMotorLeft.rotateClockWise(50);
		sideMotorRight.rotateAntiClockWise(50);
	}
	
	public void goUp(boolean movement) {
		if (movement) {
			elevation++;
		} else {
			elevation--;
		}
		try {
			if (elevation == Elevation.STOP.value()) {
				elevationMotor.stop();
			} else if (elevation == Elevation.UP.value()) {
				elevationMotor.rotateClockWise();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void goDown(boolean movement) {
		if (movement) {
			elevation--;
		} else {
			elevation++;
		}
		try {
			if (elevation == Elevation.STOP.value()) {
				elevationMotor.stop();
			} else if (elevation == Elevation.DOWN.value()) {
				elevationMotor.rotateAntiClockWise();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void goForward(boolean movement) {
		FORWARD = movement;
		try {
			if (FORWARD == BACKWARD) {
				stopMovement();
			} else if (FORWARD && LEFT && !RIGHT) {
				forwardLeftMovement();
			} else if (FORWARD && RIGHT && !LEFT) {
				forwardRightMovement();
			} else if (!FORWARD && BACKWARD) {
				backwardMovement();
			} else {
				forwardMovement();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void goBackward(boolean movement) {
		BACKWARD = movement;
		try {
			if (BACKWARD == FORWARD) {
				stopMovement();
			} else if (BACKWARD && LEFT && !RIGHT) {
				backwardLeftMovement();
			} else if (BACKWARD && RIGHT && !LEFT) {
				backwardRightMovement();
			} else if (!BACKWARD && FORWARD) {
				forwardMovement();
			} else {
				backwardMovement();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void turnLeft(boolean movement) {
		LEFT = movement;
		try {
			if (LEFT == RIGHT) {
				stopMovement();
			} else if (LEFT && FORWARD && !BACKWARD ) {
				forwardLeftMovement();
			} else if (LEFT && BACKWARD && !FORWARD) {
				backwardLeftMovement();
			} else if (!LEFT && RIGHT) {
				leftRotate();
			} else {
				rightRotate();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void turnRight(boolean movement) {
		RIGHT = movement;
		try {
			if (RIGHT == LEFT) {
				stopMovement();
			} else if (RIGHT && FORWARD && !BACKWARD ) {
				forwardRightMovement();
			} else if (RIGHT && BACKWARD && !FORWARD) {
				backwardRightMovement();
			} else if (!RIGHT && LEFT) {
				rightRotate();
			} else {
				leftRotate();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public class Motor {
		private boolean moves;
		private final int PIN_ENABLE;
		private final GpioPinDigitalOutput motorPin1;
		private final GpioPinDigitalOutput motorPin2;
		
		public Motor(int pin_enable, Pin motorPin1, Pin motorPin2) {
			PIN_ENABLE = pin_enable;
			this.motorPin1 = gpio.provisionDigitalOutputPin(motorPin1, PinState.LOW);
			this.motorPin2 = gpio.provisionDigitalOutputPin(motorPin2, PinState.LOW);
			moves = false;
			SoftPwm.softPwmCreate(PIN_ENABLE, 0, 100);
		}
		
		public void rotateClockWise(int power) throws InterruptedException {
			stop();
			motorPin1.high();
			motorPin2.low();
			SoftPwm.softPwmWrite(PIN_ENABLE, power);
			moves = true;
			
		}

		public void rotateAntiClockWise(int power) throws InterruptedException {
			stop();
			motorPin1.low();
			motorPin2.high();
			SoftPwm.softPwmWrite(PIN_ENABLE, power);
			moves = true;
		}

		public void rotateClockWise() throws InterruptedException {
			stop();
			motorPin1.high();
			motorPin2.low();
			SoftPwm.softPwmWrite(PIN_ENABLE, 100);
			moves = true;
		}
		
		public void rotateAntiClockWise() throws InterruptedException {
			stop();
			motorPin1.low();
			motorPin2.high();
			SoftPwm.softPwmWrite(PIN_ENABLE, 100);
			moves = true;
		}

		public void stop() throws InterruptedException {
			if (moves) {
				SoftPwm.softPwmWrite(PIN_ENABLE, 0);
				motorPin1.low();
				motorPin2.low();
				moves = false;
				Thread.sleep(20);
			}
		}
	}

}
